export class TodoNavBarLinks {
    constructor() {
        this.links = [
            { name: 'Angular', link: '/todo' },
            { name: 'React', link: '/todoreact' },
            { name: 'HTML', link: '/todohtml' },
            { name: 'Aurelia', link: '/todoaurelia' }
        ];
    }
    render() {
        return (h("div", { class: 'todo-nav-bar-links-div' }, this.links.map((link) => h("todo-nav-bar-link", { name: link.name, link: link.link }))));
    }
    static get is() { return "todo-nav-bar-links"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "links": {
            "state": true
        }
    }; }
    static get style() { return "/**style-placeholder:todo-nav-bar-links:**/"; }
}
