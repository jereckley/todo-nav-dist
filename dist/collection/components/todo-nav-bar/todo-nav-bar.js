export class TodoNavBar {
    render() {
        return (h("div", { class: 'todo-nav-container' },
            h("div", { class: 'todo-nav-inner' },
                h("todo-nav-bar-icon", null),
                h("todo-nav-bar-links", null))));
    }
    static get is() { return "todo-nav-bar"; }
    static get encapsulation() { return "shadow"; }
    static get style() { return "/**style-placeholder:todo-nav-bar:**/"; }
}
