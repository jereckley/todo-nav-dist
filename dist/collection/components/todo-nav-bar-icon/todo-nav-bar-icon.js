export class TodoNavBarIcon {
    render() {
        return (h("div", { class: 'to-do-nav-bar-icon-container' }, "Cross Framework"));
    }
    static get is() { return "todo-nav-bar-icon"; }
    static get encapsulation() { return "shadow"; }
    static get style() { return "/**style-placeholder:todo-nav-bar-icon:**/"; }
}
