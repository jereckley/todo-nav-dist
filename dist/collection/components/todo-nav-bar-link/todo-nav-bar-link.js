export class TodoNavBarLink {
    navigate() {
        window.location.href = this.link;
    }
    render() {
        return (h("div", { class: 'todo-nav-bar-link-div', onClick: this.navigate.bind(this) }, this.name));
    }
    static get is() { return "todo-nav-bar-link"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "link": {
            "type": String,
            "attr": "link"
        },
        "name": {
            "type": String,
            "attr": "name"
        }
    }; }
    static get style() { return "/**style-placeholder:todo-nav-bar-link:**/"; }
}
