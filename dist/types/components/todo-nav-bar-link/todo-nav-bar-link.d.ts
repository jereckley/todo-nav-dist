import '../../stencil.core';
export declare class TodoNavBarLink {
    name: string;
    link: string;
    navigate(): void;
    render(): JSX.Element;
}
