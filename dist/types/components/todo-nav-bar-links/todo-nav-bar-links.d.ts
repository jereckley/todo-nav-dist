import '../../stencil.core';
export interface Link {
    name: string;
    link: string;
}
export declare class TodoNavBarLinks {
    links: Array<Link>;
    render(): JSX.Element;
}
