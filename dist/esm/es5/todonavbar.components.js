// todonavbar: Host Data, ES Module/ES5 Target

export var TodoNavBar = ["todo-nav-bar",function(o){return(o.scoped?import("./uafcnhqb.sc.js"):import("./uafcnhqb.js")).then(function(m){return m.TodoNavBar})},1,0,1];

export var TodoNavBarIcon = ["todo-nav-bar-icon",function(o){return(o.scoped?import("./uafcnhqb.sc.js"):import("./uafcnhqb.js")).then(function(m){return m.TodoNavBarIcon})},1,0,1];

export var TodoNavBarLink = ["todo-nav-bar-link",function(o){return(o.scoped?import("./uafcnhqb.sc.js"):import("./uafcnhqb.js")).then(function(m){return m.TodoNavBarLink})},1,[["link",1,0,1,2],["name",1,0,1,2]],1];

export var TodoNavBarLinks = ["todo-nav-bar-links",function(o){return(o.scoped?import("./uafcnhqb.sc.js"):import("./uafcnhqb.js")).then(function(m){return m.TodoNavBarLinks})},1,[["links",5]],1];