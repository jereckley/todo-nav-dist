// todonavbar: Custom Elements Define Library, ES Module/ES5 Target
import { defineCustomElement } from './todonavbar.core.js';
import {
  TodoNavBar,
  TodoNavBarIcon,
  TodoNavBarLink,
  TodoNavBarLinks
} from './todonavbar.components.js';

export function defineCustomElements(window, opts) {
  defineCustomElement(window, [
    TodoNavBar,
    TodoNavBarIcon,
    TodoNavBarLink,
    TodoNavBarLinks
  ], opts);
}